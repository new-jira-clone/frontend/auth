import { config } from '@vue/test-utils';
import { createRouter, createWebHistory } from 'vue-router';
import { createPinia, setActivePinia } from 'pinia';
import useAuthRouter from '@/app/router/useAuthRouter';
import { pathConfig } from '@/shared';

function setupPinia() {
  const pinia = createPinia();

  setActivePinia(pinia);

  return pinia;
}

const pinia = setupPinia();

function setupRouter() {
  const { getRoutes, authGuard } = useAuthRouter();

  const mockDefaultRoute = {
    name: 'default',
    path: pathConfig.default,
    component: {
      template: '<div>Mock Default Page</div>',
    },
  };

  const mockMainRoute = {
    name: 'main',
    path: '/',
    component: {
      template: '<div>Mock Main Page</div>',
    },
  };

  const router = createRouter({
    history: createWebHistory(),
    routes: [...getRoutes(''), mockMainRoute, mockDefaultRoute],
  });

  router.beforeResolve((to) => authGuard(to));

  return router;
}

function fakeLoginStatus(isLogin = true) {
  Object.defineProperty(document, 'cookie', {
    writable: true,
    // TODO: вынести в функцию
    value: `SignedIn=${isLogin};`,
  });
}

config.global.stubs = {
  'router-link': true,
};

export { pinia, setupPinia, setupRouter, fakeLoginStatus };
