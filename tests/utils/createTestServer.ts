import { createServer } from 'node:http';
import { Server } from 'socket.io';
import { io as ioc } from 'socket.io-client';

interface TestServer {
  io: Server;
  serverSocket: any;
  clientSocket: any;
}

export const createTestServer = (): Promise<TestServer> => new Promise((resolve) => {
  const httpServer = createServer();
  const io = new Server(httpServer);
  let serverSocket: any;

  httpServer.listen(() => {
    const { port } = httpServer.address() as { port: number };
    const clientSocket = ioc(`http://localhost:${port}`);

    io.on('connection', (socket) => {
      serverSocket = socket;
    });

    clientSocket.on('connect', () => {
      resolve({ io, serverSocket, clientSocket });
    });
  });
});
