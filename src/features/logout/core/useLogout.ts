import { useRouter } from 'vue-router';
import { useUserStore, useSocketStore } from '@new-jira-clone/libs/src/store';
import { AuthApiService, useInitSocket } from '@/shared/api';
import { pathConfig } from '@/shared';

const useLogout = () => {
  const { setUser } = useUserStore();
  const router = useRouter();

  const closeSocketConnection = () => {
    const { socket, setSocket } = useSocketStore();

    if (socket.value) {
      socket.value.close();
      setSocket(null);
    }
  };

  const resetUser = () => setUser(null);

  const redirectToLoginPage = () => router.push(pathConfig.login);

  const logout = async () => {
    try {
      await AuthApiService.logout();
    } catch (error) {
      console.log(error);
    } finally {
      const { setSignInCookieToFalse } = useInitSocket();

      closeSocketConnection();
      setSignInCookieToFalse();
      resetUser();
      redirectToLoginPage();
    }
  };

  return {
    logout,
  };
};

export default useLogout;
