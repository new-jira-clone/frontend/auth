import { useRouter } from 'vue-router';
import { useUserStore } from '@new-jira-clone/libs/src/store';
import { AuthApiService, useInitSocket } from '@/shared/api';
import { type LoginByEmailForm, type User } from '@/shared/api';
import { pathConfig } from '@/shared';

export default () => {
  const router = useRouter();
  const { setUser } = useUserStore();
  const { initSocketConnect } = useInitSocket();

  const loginByEmail = async (form: LoginByEmailForm) => {
    const result = await AuthApiService.loginByEmail<User>(form);

    if (result.status === 200) {
      setUser(result.data);
      initSocketConnect();
      router.push(pathConfig.default);
    }
  };

  return {
    loginByEmail,
  };
};
