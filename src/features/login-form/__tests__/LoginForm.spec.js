import { flushPromises, mount } from '@vue/test-utils';
import { beforeEach, describe, expect, it, vi } from 'vitest';
import { Ripple } from '@new-jira-clone/ui/src/directives/ripple';
import LoginForm from '../ui/LoginForm.vue';

describe('LoginForm', () => {
  const mockI18n = vi.fn((key) => key);
  const mockSubmitForm = vi.fn();
  const mockRejectSubmitForm = vi.fn().mockRejectedValue();
  const mockSubmitFormWithDelay = vi.fn().mockImplementation(() => {
    const delay = 100;

    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, delay);
    });
  });

  let wrapper;

  beforeEach(() => {
    vi.clearAllMocks();

    wrapper = mount(LoginForm, {
      global: {
        provide: {
          $t: mockI18n,
        },
        directives: {
          ripple: Ripple,
        },
      },
      props: {
        submitForm: mockSubmitForm,
      },
    });
  });

  describe('Validate', () => {
    let emailInput;
    let passwordInput;

    beforeEach(() => {
      emailInput = wrapper.get('input[type="email"]');
      passwordInput = wrapper.get('input[type="password"]');
    });

    it('should return errors if email and password is empty', async () => {
      await wrapper.get('form').trigger('submit');

      expect(wrapper.find('[data-test-email-error]').exists()).toBe(true);
      expect(wrapper.find('[data-test-password-error]').exists()).toBe(true);
      expect(mockSubmitForm).not.toHaveBeenCalled();
    });

    it('should return error if email is invalid', async () => {
      await emailInput.setValue('invalid-email');
      await emailInput.trigger('blur');

      expect(wrapper.find('[data-test-email-error]').exists()).toBe(true);
    });

    it('should return error if email is short', async () => {
      await emailInput.setValue('123');
      await emailInput.trigger('blur');

      expect(wrapper.find('[data-test-email-error]').exists()).toBe(true);
    });

    it('should return error if password is short', async () => {
      await passwordInput.setValue('123');
      await passwordInput.trigger('blur');

      expect(wrapper.find('[data-test-password-error]').exists()).toBe(true);
    });

    it('should reset email errors on changing value to correct', async () => {
      await emailInput.setValue('wrong-email');
      await emailInput.trigger('blur');

      expect(wrapper.find('[data-test-email-error]').exists()).toBe(true);

      await emailInput.setValue('correct@email.com');

      expect(wrapper.find('[data-test-email-error]').exists()).toBe(false);
    });

    it('should reset password errors on changing value to correct', async () => {
      await passwordInput.setValue('123');
      await passwordInput.trigger('blur');

      expect(wrapper.find('[data-test-password-error]').exists()).toBe(true);

      await passwordInput.setValue('1234');

      expect(wrapper.find('[data-test-password-error]').exists()).toBe(false);
    });
  });

  describe('Submit', () => {
    const validForm = {
      email: 'test@mail.com',
      password: 'password',
    };
    let emailInput;
    let passwordInput;

    beforeEach(() => {
      emailInput = wrapper.get('input[type="email"]');
      passwordInput = wrapper.get('input[type="password"]');
    });

    it('should be successfully submit form with valid data', async () => {
      await emailInput.setValue(validForm.email);
      await passwordInput.setValue(validForm.password);

      await wrapper.get('form').trigger('submit');

      await flushPromises();

      expect(mockSubmitForm).toHaveBeenCalledWith(validForm);
    });

    it('should be once call submit form function', async () => {
      await wrapper.setProps({
        submitForm: mockSubmitFormWithDelay,
      });

      await emailInput.setValue(validForm.email);
      await passwordInput.setValue(validForm.password);

      await wrapper.get('form').trigger('submit');

      await flushPromises();

      await wrapper.get('form').trigger('submit');
      await wrapper.get('form').trigger('submit');

      expect(mockSubmitFormWithDelay).toHaveBeenCalledOnce();
    });

    it('should not submit with invalid email format', async () => {
      await wrapper.setProps({
        submitForm: mockRejectSubmitForm,
      });

      await emailInput.setValue('exist@email.com');
      await passwordInput.setValue('bad-password');

      await wrapper.get('form').trigger('submit');

      await flushPromises();

      expect(mockSubmitForm).not.toHaveBeenCalled();
      expect(wrapper.get('[data-test-response-errors]').exists()).toBe(true);
    });
  });
});
