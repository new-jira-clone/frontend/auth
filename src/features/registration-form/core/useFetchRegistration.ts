import { useRouter } from 'vue-router';
import { useUserStore } from '@new-jira-clone/libs/src/store';
import { useInitSocket, RegistrationForm, AuthApiService } from '@/shared/api';
import { pathConfig } from '@/shared';

const useRegistration = () => {
  const router = useRouter();
  const { setUser } = useUserStore();
  const { initSocketConnect } = useInitSocket();

  const submitRegistrationForm = async (form: RegistrationForm) => {
    const result = await AuthApiService.register(form);

    if (result.status === 201) {
      setUser(result.data);
      initSocketConnect();
      router.push(pathConfig.default);
    }
  };

  return {
    submitRegistrationForm,
  };
};

export default useRegistration;
