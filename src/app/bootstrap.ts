import { createApp } from 'vue';
import App from './AuthApp.vue';

createApp(App).mount('#app');
