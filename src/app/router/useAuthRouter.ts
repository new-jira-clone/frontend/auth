import { ComponentPublicInstance } from 'vue';
import { RouteLocationNormalized } from 'vue-router';
import { storeToRefs } from 'pinia';
import { useUserStore } from '@new-jira-clone/libs/src/store';
import { useInitSocket } from '@/shared/api';
import { pathConfig } from '@/shared';

export default () => {
  const authGuard = (to: RouteLocationNormalized) => {
    const { isUserAuth } = storeToRefs(useUserStore());
    const { isSignedInCookie } = useInitSocket();

    if (to.meta.requiresAuth !== false && (isUserAuth.value || isSignedInCookie())) {
      return true;
    }

    if (
      to.meta.requiresAuth === false
      && to.meta.authPage === true
      && (isUserAuth.value || isSignedInCookie())
    ) {
      return { path: pathConfig.default, force: true };
    }

    if (to.meta.requiresAuth === false) {
      return true;
    }

    return pathConfig.login;
  };

  const getRoutes = (layout: ComponentPublicInstance) => [
    {
      path: pathConfig.login,
      name: 'login',
      component: () => import('@/pages/login/index.vue'),
      meta: {
        layout,
        requiresAuth: false,
        authPage: true,
      },
    },
    {
      path: pathConfig.register,
      name: 'registration',
      component: () => import('@/pages/register/index.vue'),
      meta: {
        layout,
        requiresAuth: false,
        authPage: true,
      },
    },
  ];

  const getPathConfig = pathConfig;

  return {
    getRoutes,
    authGuard,
    getPathConfig,
  };
};
