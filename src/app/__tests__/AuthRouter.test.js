import { mount } from '@vue/test-utils';
import { useUserStore } from '@new-jira-clone/libs/src/store';
import { describe, it, expect, beforeEach, vi, beforeAll, afterAll } from 'vitest';
import { setupRouter, fakeLoginStatus } from '@tests/setupTests';
import { createTestServer } from '@tests/utils/createTestServer';
import { createTestingPinia } from '@pinia/testing';
import { pathConfig } from '@/shared';
import useAuthRouter from '../router/useAuthRouter';
import AuthApp from '../AuthApp.vue';

describe('Auth router', () => {
  let router;

  const addFakeRoute = (requiresAuth) => {
    const fakeComponent = AuthApp;
    const fakePagePath = '/test-page-check-requiresAuth';

    router.addRoute({
      path: fakePagePath,
      component: fakeComponent,
      meta: { requiresAuth },
    });

    return fakePagePath;
  };

  beforeEach(async () => {
    const { authGuard } = useAuthRouter();

    router = setupRouter();

    router.push(pathConfig.default);

    router.beforeResolve((to) => authGuard(to));

    mount(AuthApp, {
      global: {
        plugins: [router],
      },
    });

    await router.isReady();
  });

  describe('If user not signed in:', () => {
    it('redirect to a login page', async () => {
      expect(router.currentRoute.value.path).toBe(pathConfig.login);
    });

    it('can be visit to a register page', async () => {
      await router.push(pathConfig.register);

      expect(router.currentRoute.value.path).toBe(pathConfig.register);
    });

    it('redirect to a login page if a target page has requiresAuth meta parameter', async () => {
      const createdRoute = addFakeRoute(true);

      await router.push(createdRoute);

      expect(router.currentRoute.value.path).toBe(pathConfig.login);
    });

    it('can be visit to a page if the page has "requiresAuth" param in the value `false`', async () => {
      const createdRoute = addFakeRoute(false);

      await router.push(createdRoute);

      expect(router.currentRoute.value.path).toBe(createdRoute);
    });
  });

  describe('If user signed in:', () => {
    let testServer;

    beforeAll(async () => {
      fakeLoginStatus();
      testServer = await createTestServer();
    });

    afterAll(() => {
      testServer.io.close();
      testServer.clientSocket.disconnect();
    });

    it('redirect to default page with auth page', async () => {
      await router.push(pathConfig.login);

      expect(router.currentRoute.value.path).toBe(pathConfig.default);
    });

    it('can be visit page requiring auth', async () => {
      const createdRoute = addFakeRoute(true);

      await router.push(createdRoute);

      expect(router.currentRoute.value.path).toBe(createdRoute);
    });

    it('can be visit page without requiring auth', async () => {
      const createdRoute = addFakeRoute(true);

      await router.push(createdRoute);

      expect(router.currentRoute.value.path).toBe(createdRoute);
    });

    it('can be visit page requiring auth if has only user value', async () => {
      fakeLoginStatus(false);

      createTestingPinia();

      const store = useUserStore();

      store.isUserAuth = true;

      const createdRoute = addFakeRoute(true);

      await router.push(createdRoute);

      expect(router.currentRoute.value.path).toBe(createdRoute);
    });
  });
});
