export interface LoginByEmailForm {
  email: string;
  password: string;
}

export interface RegistrationForm {
  name: string;
  email: string;
  password: string;
}
