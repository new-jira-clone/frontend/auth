import { type LoginByEmailForm, type RegistrationForm } from '../types/Auth.interface';
import axios from './AxiosInstance';

export default {
  async loginByEmail<T>(form: LoginByEmailForm) {
    return axios.post<T>('/api/auth/login', form);
  },

  async register(form: RegistrationForm) {
    return axios.post('/api/auth/register', form);
  },

  async checkAuth() {
    return axios.get('/api/auth');
  },

  async logout() {
    return axios.get('/api/auth/logout');
  },

  async guestsList() {
    return axios.get('/api/auth/guests-list');
  },

  async guestSignIn(userEmail: string) {
    return axios.post('/api/auth/guest-sign-in', {
      email: userEmail,
    });
  },
};
