import { ref } from 'vue';
import { useRouter } from 'vue-router';
import { io } from 'socket.io-client';
import { useSocketStore, useUserStore } from '@new-jira-clone/libs/src/store';
import { type User } from '../types/User.interface';

const isInitConnect = ref(false);

export default () => {
  const router = useRouter();

  const { setUser } = useUserStore();
  const { setSocket } = useSocketStore();

  const isSignedInCookie = () => document.cookie.includes('SignedIn=true');

  // TODO: remove manual logout and remake to a server-side
  const setSignInCookieToFalse = () => {
    document.cookie = `SignedIn=false; path=/; Domain=.${window.location.host};`;
  };

  const resetSocket = () => {
    isInitConnect.value = true;
    setSocket(null);
  };

  const initSocketConnect = () => {
    const newSocket = io(process.env.VUE_APP_SERVER_WS ?? '', {
      withCredentials: true,
      transports: ['websocket'],
    });

    newSocket.on('connection', (user: User) => {
      setSocket(newSocket);
      setUser(user);
      isInitConnect.value = true;
    });

    newSocket.on('authFailed', () => {
      setSignInCookieToFalse();
      setUser(null);
      router.push({ name: 'login' });
      resetSocket();
      newSocket.disconnect();
    });
  };

  const initSocketIfUserSigned = () => {
    if (isSignedInCookie()) {
      initSocketConnect();
    } else {
      resetSocket();
    }
  };

  return {
    isInitConnect,
    initSocketConnect,
    isSignedInCookie,
    initSocketIfUserSigned,
    setSignInCookieToFalse,
  };
};
