import {
  required as vRequired,
  email as vEmail,
  minLength as vMinLength,
  helpers,
} from '@vuelidate/validators';

// TODO: add i18n
const required = helpers.withMessage('This field is required', vRequired);
const email = helpers.withMessage('Email not valid', vEmail);
const minLength = (length: number) => helpers.withMessage(
  `This field should be at least ${length} characters long`,
  vMinLength(length),
);

// TODO: fix any
const getError = (formControl: any) => formControl?.$errors.map((error: any) => error.$message);

export { minLength, required, email, getError };
