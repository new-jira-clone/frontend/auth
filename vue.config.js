/* eslint-disable import/no-extraneous-dependencies */

const path = require('path');
const { defineConfig } = require('@vue/cli-service');
const { ModuleFederationPlugin } = require('webpack').container;
const dev = require('./package.json');

module.exports = defineConfig({
  publicPath: process.env.VUE_APP_AUTH,
  transpileDependencies: true,
  configureWebpack: {
    optimization: {
      splitChunks: false,
    },
    entry: path.resolve(__dirname, './src/index.ts'),
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
        '@tests': path.resolve(__dirname, './tests'),
      },
    },
    plugins: [
      new ModuleFederationPlugin({
        name: 'Auth',
        filename: 'remoteEntry.js',
        exposes: {
          './features/logout': './src/features/logout/index.ts',
          './router': './src/app/router/useAuthRouter.ts',
          './AuthApp': './src/app/AuthApp.vue',
        },
        shared: {
          ...dev.dependencies,
          axios: {
            singleton: true,
            requiredVersion: dev.dependencies.axios,
          },
          vue: {
            eager: true,
            singleton: true,
            requiredVersion: dev.dependencies.vue,
          },
          '@new-jira-clone/libs/': {
            eager: false,
            singleton: true,
            strictVersion: false,
            requiredVersion: dev.dependencies['@new-jira-clone/libs'],
          },
          '@new-jira-clone/ui/': {
            eager: false,
            singleton: true,
            strictVersion: false,
            requiredVersion: dev.dependencies['@new-jira-clone/ui'],
          },
        },
      }),
    ],
    devServer: {
      port: 3003,
    },
  },
});
