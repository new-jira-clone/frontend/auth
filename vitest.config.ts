/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from 'vitest/config';
import vue from '@vitejs/plugin-vue';
import path from 'path';

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@new-jira-clone/libs': path.resolve(__dirname, 'node_modules/@new-jira-clone/libs'),
      '@new-jira-clone/ui': path.resolve(__dirname, 'node_modules/@new-jira-clone/ui'),
      '@': path.resolve(__dirname, 'src'),
      '@tests': path.resolve(__dirname, 'tests'),
    },
  },
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: './tests/setupTests.js',
    deps: {
      moduleDirectories: ['node_modules', path.resolve('../../package.json')],
    },
  },
});
